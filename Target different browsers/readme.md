# Intro

If you need to apply different CSS rules, based on browser (for example something needs to be fixed on IE or Edge), there is a function that ADD a class to the BODY with the name of the browser that you're using.

# How to

## Step 1
Open **Appearance > Editor > function.php** and paste the code you find in `broswer-target.php` code after `<?php`


## Step 2
After you just need to target the element you need to fix using the body tag with the class of the browser before your rule.

Example

```
.banana{
    margin-right:20px;
}
body.is_IE .banana{
    margin-right:50px;
    background-color:red;
}
```

In this example we have a class called 'banana', with a margin right of 20 pixels.
The same class on IE will have a margin right of 50 pixels and a red background.